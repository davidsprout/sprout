import json
from collections import defaultdict

import pandas as pd


class Allocator:
    def __init__(self, tasks, employees):
        self._allocations = defaultdict(lambda: defaultdict(list))
        self._tasks = tasks
        self._employee_to_job = employees
        self._job_to_emploee = defaultdict(set)
        for person_name, person_job in self._employee_to_job.items():
            self._job_to_emploee[person_job].add(person_name)
        self._unallocatable = []

    def get_allocations(self):
        return self._allocations, self._unallocatable

    @staticmethod
    def data_loader(tasks_path, employees_path):
        tasks = pd.read_csv(tasks_path).replace({'None': None})
        with open(employees_path) as employees_file:
            employees_text = employees_file.read()
            employees_text = employees_text.replace("'", '"')
            employees = json.loads(employees_text)
        return tasks, employees

    def allocate(self):
        """
        Simple implementation of the task allocation exercise.

        :return: None
        """

        unallocated = defaultdict(list)

        # keep the allocated tasks as assigned by the management
        for _, task in self._tasks.iterrows():
            # keep the management assignments if the task was given to the right employee
            if task['user'] is not None and task['type'] == self._employee_to_job[task['user']]:
                self._allocations[task['type']][task['user']].append(task)
            elif task['type'] in self._job_to_emploee.keys():
                # for each unallocated task there is an employee who can take them on
                unallocated[task['type']].append(task)
            else:
                # if there are no employees for some tasks, report them as unallocatable
                self._unallocatable.append(task)

        for job in self._job_to_emploee.keys():
            self._allocate_one_type(unallocated[job], self._job_to_emploee[job], self._allocations[job])

    @staticmethod
    def _allocate_one_type(unallocated, employees, allocations):
        # distribute some of the unallocated tasks to even the load per person
        loads_per_person = [len(allocations[employee]) for employee in employees]
        max_load = max(loads_per_person)
        min_load = min(loads_per_person)
        while len(unallocated) > 0 and max_load > min_load:
            # there are tasks to allocate and not all people have the same load
            for person_name in employees:
                person_load = allocations[person_name]
                if len(person_load) < max_load:
                    person_load.append(unallocated.pop())
                if len(unallocated) == 0:
                    break
                loads_per_person = [len(allocation) for allocation in allocations.values()]
                min_load = min(loads_per_person)

        # now all people have equal task allocations or as close to it as we can get
        # if there were not enough tasks to achieve equal allocations
        if len(unallocated) > 0:
            while len(unallocated) > 0:
                # there are more tasks to allocate
                for person_name in employees:
                    person_load = allocations[person_name]
                    person_load.append(unallocated.pop())
                    if len(unallocated) == 0:
                        break

    @staticmethod
    def _task_as_str(task):
        return ','.join([task['task_id'], task['type'], task['description'], task['version'], str(task['user'])])

    def __str__(self):
        as_string_list = []
        # reprot all allocates tasks per person
        for person_name, person_job in self._employee_to_job.items():
            person_load = self._allocations[person_job][person_name]
            as_string_list.append(person_name + ' ' + person_job + ' - ' + str(len(person_load)) + ' tasks')
            for task in person_load:
                as_string_list.append(self._task_as_str(task))
        # report all unallocated tasks
        if len(self._unallocatable) > 0:
            as_string_list.append('Unallocated tasks')
            for task in self._unallocatable:
                as_string_list.append(self._task_as_str(task))
        return '\n'.join(as_string_list)
