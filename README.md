# Description
This solution was developed in PyCharm 2021.1.1. The run configurations are saved in the .run directory.

There is one Run configuration to execute the top level script task_allocations.py. This script processes the provided
data files (saved under the `data` directory) and prints out the results.

There is one test under the `tests` directory. It is executed by the test Run configurations.

The dependencies of this code are listed in `requirements.txt`. The Pycharm Run configurations expect the virtualenv
to be in a subdirectory called `venv`.

The code was tested with python 3.9.
