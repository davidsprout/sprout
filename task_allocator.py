import os.path
from pathlib import Path

import sprout.allocator

if __name__ == "__main__":
    # load data for task 1
    data_dir = Path(os.path.dirname(__file__))/'data'
    employees_path = data_dir/'employees.txt'
    tasks_path = data_dir/'tasks.csv'
    tasks, employees = sprout.allocator.Allocator.data_loader(tasks_path, employees_path)
    # run the allocations
    allocator = sprout.allocator.Allocator(tasks, employees)
    allocator.allocate()
    # print out the task-to-employee assignments
    print(allocator)
