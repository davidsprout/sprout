import os
from pathlib import Path

import sprout.allocator


def test_allocate():
    # load the test data
    data_dir = Path(os.path.dirname(__file__))/'data'
    employees_path = data_dir/'employees.txt'
    tasks_path = data_dir/'tasks.csv'
    tasks, employees = sprout.allocator.Allocator.data_loader(tasks_path, employees_path)
    # run the allocations
    allocator = sprout.allocator.Allocator(tasks, employees)
    allocator.allocate()

    # validate the output
    allocations, unallocatable = allocator.get_allocations()
    assert 'Person One' in allocations['engineer'].keys()
    assert 'Person Two' in allocations['engineer'].keys()
    assert len(allocations['engineer']['Person One']) == 1
    assert len(allocations['engineer']['Person Two']) == 2
    assert '557958ef-9129-46f2-a64d-65c83d4670c7' in [task['task_id'] for task in allocations['engineer']['Person One']]
    assert len(unallocatable) == 1
